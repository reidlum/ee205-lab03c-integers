///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 03c - Integers - EE 205 - Spr 2022
///
/// @file integers.h
/// @version 1.0
///
/// @author Reid Lum <reidlum@hawaii.edu>
/// @date   29 Jan 2022
///////////////////////////////////////////////////////////////////////////////

#define TABLE_HEADER1 "Datatype       bits bytes              Minimum              Maximum\n"
#define TABLE_HEADER2 "-------------- ---- ----- -------------------- --------------------\n"
#define TABLE_FORMAT_CHAR  "%-14s %4d %5d %20d %20d\n"
#define TABLE_FORMAT_SHORT "%-14s %4ld %5ld %20d %20d\n"
#define TABLE_FORMAT_INT   "%-14s %4ld %5ld %20d %20d\n"
#define TABLE_FORMAT_UINT  "%-14s %4ld %5ld %20u %20u\n"
#define TABLE_FORMAT_LONG  "%-14s %4ld %5ld %20ld %20ld\n"
#define TABLE_FORMAT_ULONG "%-14s %4ld %5ld %20u %20lu\n"  
//Im not sure if we were supposed to mess with this code but I changed the 20lu to 20u because
//thats the only way I found to correct the copmlier warnings

